import React, { useState } from 'react';
import Title from './Title'
import SearchForm from './SearchForm'
import MoviesList from './MoviesList'

const Home = () => {
  const [results, setResult] = useState([]);
  const [usedSearch, setUsedSearch] = useState(false)
    ;

  const handleResults = (results) => {
    setResult(results);
    setUsedSearch(true);
  }

  const _renderResult = () => {
    return results.length === 0
      ? <p> Sorry! Result not found</p>
      : <MoviesList movies={results} />
  }

  return (
  <div>
    <Title>Search Movies</Title>
      <div className='SearchForm-wrapper'>
        <SearchForm onResults={handleResults} />
      </div>
      {
        usedSearch
          ? _renderResult()
          : <small>Use the form to search a movie </small>
      }
  </div>
  );
}

export default Home;

