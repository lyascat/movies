import React from 'react';

import { Switch, Route } from 'react-router-dom'

import Home from './components/Home'
import Detail from './pages/Detail'
import NotFound from './pages/NotFound'


import './App.css';
import 'bulma/css/bulma.css'

function App() {

  /* const url = new URL(document.location);

  const Page = url.searchParams.has('id')

  ? <Detail id ={url.searchParams.get('id')}/>
  : <Home /> */

  return (
    <div className="App">
    <Switch>
      <Route exact path='/' component={Home} />
      <Route path='/detail/:moveID' component= {Detail} />
      <Route component = {NotFound}/>
    </Switch>

    </div>
  );
}

export default App;
