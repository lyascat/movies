import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import ButtonBackToHome from '../components/ButtonBackToHome'

const API_KEY = '81d80893';

const Detail = (props) => {

  const [movie, setMovie] = useState({});

  const _fetchMovie = ({ id }) =>{
    fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
    .then(res => res.json())
    .then(movie =>{
        console.log('aqui',{ movie })
        setMovie(movie);
    })
  }



  useEffect(() => {
    console.log(props)
    const { moveID } = props.match.params;
    _fetchMovie({id: moveID});
  },[])

  const {Title, Poster, Actors, Metascore, Plot } = movie;

  return(
    <div>
      <ButtonBackToHome />
      <h1> {Title}</h1>
      <img src ={Poster} alt =''/>
      <h3>{Actors}</h3>
      <span>{Metascore}</span>
      <p>{Plot}</p>
    </div>

  )
}

Detail.prototype = {

  match: PropTypes.shape({
    params: PropTypes.object,
    isExact: PropTypes.bool,
    path: PropTypes.string,
    url: PropTypes.string
  })
}


export default Detail;
